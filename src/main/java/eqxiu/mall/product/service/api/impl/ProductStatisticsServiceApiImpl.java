package eqxiu.mall.product.service.api.impl;


import eqxiu.mall.product.dao.ProductStatisticsDao;
import eqxiu.mall.product.dto.ProductStatisticsDTO;
import eqxiu.mall.product.model.ProductStatistics;
import eqxiu.mall.product.service.api.ProductStatisticsApi;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductStatisticsServiceApiImpl implements ProductStatisticsApi {

    @Autowired
    private ProductStatisticsDao statisticsDao;

    @Override
    public ProductStatisticsDTO getById(Long id) {

        ProductStatistics model = statisticsDao.selectByPrimaryKey(id);

        if(model == null){
            return null;
        }
    
        ProductStatisticsDTO dto = new ProductStatisticsDTO();
        BeanUtils.copyProperties(model,dto);
        return dto;

    }
}
