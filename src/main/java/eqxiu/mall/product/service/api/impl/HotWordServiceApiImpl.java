package eqxiu.mall.product.service.api.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import eqxiu.mall.product.dto.HotWordDTO;
import eqxiu.mall.product.exception.ApiException;
import eqxiu.mall.product.model.HotWord;
import eqxiu.mall.product.model.HotWordExample;
import eqxiu.mall.product.service.HotWordService;
import eqxiu.mall.product.service.api.HotWordServiceApi;

public class HotWordServiceApiImpl implements HotWordServiceApi {

	@Autowired
	private HotWordService hotWordService;

	@Override
	public List<HotWordDTO> getHotWordsByCategory(Integer categoryId) throws ApiException {
		if (categoryId == null) {
			throw new ApiException("categoryId is null");
		}

		HotWordExample example = new HotWordExample();
		example.setOrderByClause("sort desc");
		example.createCriteria().andCategoryEqualTo(categoryId.byteValue());
		List<HotWord> hotWords = hotWordService.getHotWords(example);
		List<HotWordDTO> dtos = new ArrayList<HotWordDTO>();
		
		for (HotWord hw : hotWords) {
			HotWordDTO dto = new HotWordDTO();
			BeanUtils.copyProperties(hw, dto);
			dtos.add(dto);
		}
		
		return dtos;
	}

}
