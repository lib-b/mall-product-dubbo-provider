package eqxiu.mall.product.service.api.impl;


import com.fasterxml.jackson.databind.ObjectMapper;
import eqxiu.mall.async.dto.AsyncUserDTO;
import eqxiu.mall.async.dto.CallBackStatusDto;
import eqxiu.mall.async.dto.ProductContentDTO;
import eqxiu.mall.async.dto.ProductDTO;
import eqxiu.mall.async.form.ProductFormBean;
import eqxiu.mall.async.form.SceneForm;
import eqxiu.mall.commons.constants.HotWordCategoryEnum;
import eqxiu.mall.commons.constants.OrderStatusEnum;
import eqxiu.mall.commons.web.MsgCode;
import eqxiu.mall.order.event.AsyncCreateEvent;
import eqxiu.mall.order.model.Order;
import eqxiu.mall.order.model.OrderCoupon;
import eqxiu.mall.order.model.OrderExample;
import eqxiu.mall.order.service.OrderCouponService;
import eqxiu.mall.order.service.OrderService;
import eqxiu.mall.product.dao.ProductAttributeDao;
import eqxiu.mall.product.dto.AsyncOrderCallbackDTO;
import eqxiu.mall.product.dto.ProductFormDTO;
import eqxiu.mall.product.dto.SceneFormDto;
import eqxiu.mall.product.dto.UserDTO;
import eqxiu.mall.product.model.*;
import eqxiu.mall.product.service.AsyncOrderCallbackService;
import eqxiu.mall.product.service.AttributeKeyService;
import eqxiu.mall.product.service.ProductService;
import eqxiu.mall.product.service.api.AsyncOrderCallbackServiceApi;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class AsyncOrderCallbackServiceApiImpl implements AsyncOrderCallbackServiceApi {

    @Autowired
    private AsyncOrderCallbackService callbackService;

    @Resource(name="mallRedisCache")
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private ProductService productService;

    @Autowired
    private AttributeKeyService attributeKeyService;

    @Autowired
    private ProductAttributeDao productAttributeDao;

    @Autowired
    private AsyncOrderCallbackService asyncOrderCallbackService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderCouponService orderCouponService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public int updateByOrderId(AsyncOrderCallbackDTO dto) {
        AsyncOrderCallback callback = new AsyncOrderCallback();
        BeanUtils.copyProperties(dto, callback);
        callback.setOrderId(null);
        AsyncOrderCallbackExample example = new AsyncOrderCallbackExample();
        example.createCriteria().andOrderIdEqualTo(dto.getOrderId());
        callbackService.updateByExampleSelective(callback,example);
        return 0;
    }

    @Override
    public int updateById(AsyncOrderCallbackDTO dto) {
        AsyncOrderCallback callback = new AsyncOrderCallback();
        BeanUtils.copyProperties(dto, callback);
        callbackService.updateByPrimaryKeySelective(callback);
        return 0;
    }

    @Override
    public Map<String, Object> callback(ProductFormDTO product, Integer orderId, Integer prodType, Integer source, UserDTO user) {
        Map<String, Object> result = new HashMap<String, Object>();
        if(prodType== null){
            result.put("error", "商品类型不能为空");
            return result;
        }

        //校验字体为空的licenese不能为空
        if(prodType == HotWordCategoryEnum.FONT.getCode() && product.getLicense() ==null){
            result.put("error", "字体license不能为空");
            return result;
        }

        if(product == null ||product.getId() == null){
            result.put("error", "产品ID不能为空");
            return result;
        }

        if(source == null){
            result.put("error", "source不能为空");
            return result;
        }

        //用户不能为空
        AsyncUserDTO userDto = new AsyncUserDTO();
        if (user == null) {
            result.put("error", "获取用户信息失败");
            return result;
        }else{
            BeanUtils.copyProperties(user,userDto);
        }

        String cacheKey = "product:tpl_" + prodType + "_"  + user.getId() + "_" + product.getId();

        //如果订单为空，校验是否为免费商品，如果订单不为空，校验订单是否有效
        if(orderId ==null){

            //企业版字体校验字体是否为0的逻辑
            if(prodType == HotWordCategoryEnum.FONT.getCode() && product.getLicense() == 1){
                AttributeKey key = attributeKeyService.selectByName("business_price");
                if(key != null && key.getId() != null){
                    ProductAttributeExample attrExample = new ProductAttributeExample();
                    attrExample.createCriteria().andProductIdEqualTo(product.getId()).andAttrKeyIdEqualTo(key.getId());
                    List<ProductAttribute> attrList = productAttributeDao.selectByExample(attrExample);
                    if(attrList != null && attrList.size() >0){
                        try {
                            Double business_price =  Double.parseDouble((String)attrList.get(0).getAttrValue());
                            product.setPrice(business_price);
                        }catch (Exception e){
                            product.setPrice(product.getPrice());
                            e.printStackTrace();
                        }
                    }
                }
                //图片样例音乐 免费字体
            }else{
                //如果订单不存在，则校验是否是免费商品，如果freeCount为0说明订单异常
                ProductExample pExpl = new ProductExample();
                pExpl.createCriteria().andIdEqualTo(product.getId()).andPriceIsNullOrZero();
                long freeCount = productService.countByExample(pExpl);
                if(freeCount == 0){
                    result.put("error", "订单异常");
                    return result;
                }
                product.setPrice(new Double(0));
            }

        }else{
            OrderExample example = new OrderExample();
            example.createCriteria().andIdEqualTo(orderId).andProductIdEqualTo(product.getId()).andStatusEqualTo(OrderStatusEnum.PAID.status());;
            List<Order> orders = orderService.selectByExample(example);
            if(orders == null || orders.size() == 0){
                result.put("error", "订单不存在或未支付");
                return result;
            }else{
                //企业字体需要查对应的扩展属性，如果取数，转型有异常，用表单提交的价格
                if(prodType == HotWordCategoryEnum.FONT.getCode() && product.getLicense() == 1){
                    AttributeKey key = attributeKeyService.selectByName("business_price");
                    if(key != null && key.getId() != null){
                        ProductAttributeExample attrExample = new ProductAttributeExample();
                        attrExample.createCriteria().andProductIdEqualTo(product.getId()).andAttrKeyIdEqualTo(key.getId());
                        List<ProductAttribute>  attrList = productAttributeDao.selectByExample(attrExample);
                        if(attrList != null && attrList.size() >0){
                            try {
                                Double business_price =  Double.parseDouble((String)attrList.get(0).getAttrValue());
                                product.setPrice(business_price);
                            }catch (Exception e){
                                product.setPrice(product.getPrice());
                                e.printStackTrace();
                            }
                        }else{
                            product.setPrice(product.getPrice());
                        }
                    }
                }else{
                    Order o = orders.get(0);
                    product.setOrderType(o.getContent());
                    if(o.getTotalFee() == null){
                        product.setPrice(new Double(0));
                    }else{
                        product.setPrice(o.getTotalFee().doubleValue());
                    }

                }

            }
        }

        logger.info("callback invoked!product param is  "+product.toString() +" orderId is " + orderId + " prodType is "+ prodType);

        Integer mallID = product.getId();

        //字体需要把name sort改为从库里查
        if(prodType == HotWordCategoryEnum.FONT.getCode()){
            Product p = productService.getProduct(product.getId());
            product.setName(p.getTitle());
            if(p.getSort() == null){
                product.setSort(0);
            }else{
                product.setSort(p.getSort());
            }
        }

        //获取图片音乐的path
        if(prodType == HotWordCategoryEnum.IMAGE.getCode() || prodType == HotWordCategoryEnum.MUSIC.getCode()){
            Integer r = getPath(product,prodType);
            if(r == 0){
                result.put("error", "该商品找不到path属性");
                return result;
            }
        }


        //字体，样例，音乐 需要把mysql端ID转为oracle端ID
        if(prodType != HotWordCategoryEnum.IMAGE.getCode()){
            Product p = productService.getProduct(product.getId());
            if(p.getSourceId() == null){
                logger.info("source_id为空 "+p.toString());
                result.put("error", "商品的source_id为空");
                return result;
            }
            product.setId(p.getSourceId());
        }


        if (redisTemplate.opsForValue().get(cacheKey) != null) {
            result.put("error", "重复提交");
            return result;
        } else {
            redisTemplate.opsForSet().add(cacheKey,"1");
           /* Long opFlag = redisTemplate.opsForSet().add("cacheKey","1");
            if(opFlag == 0){
                result.put("error", "重复提交");
                return result;
            }*/
        }

        OrderCoupon orderCoupon = null;
        if(orderId != null){
            orderCoupon = orderCouponService.selectByPrimaryKey(orderId);
        }


        AsyncOrderCallback asyncOrderCallback = new AsyncOrderCallback();
        asyncOrderCallback.setType(prodType.byteValue());
        asyncOrderCallback.setOrderId(orderId);
        asyncOrderCallback.setProductId(mallID);
        asyncOrderCallback.setStatus(new Integer(0).byteValue());
        asyncOrderCallback.setCreateTime(new Date());
        asyncOrderCallback.setCreateUser(user.getLoginName());
        asyncOrderCallback.setSource(source.byteValue());

        logger.info("after convert.AsyncOrderCallback param is "+asyncOrderCallback.toString());

        try {
            if(product.getUseFlag() != null && product.getUseFlag() == true){
                asyncOrderCallback.setUseFlag(new Integer(1).byteValue());
            }else {
                asyncOrderCallback.setUseFlag(new Integer(0).byteValue());
            }
            asyncOrderCallbackService.insertSelective(asyncOrderCallback);
            logger.info("callback save success");
            String cacheKeyForOrder = "asyncOrderCallback:"+asyncOrderCallback.getId();
            CallBackStatusDto statusDto = new CallBackStatusDto();
            statusDto.setStatus(0);
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(statusDto);
            redisTemplate.opsForValue().set(cacheKeyForOrder,json,10, TimeUnit.MINUTES);

            ProductDTO dto = new ProductDTO();
            dto.setUser(userDto);
            dto.setType(prodType);
            ProductContentDTO contentDTO = new ProductContentDTO();
            BeanUtils.copyProperties(product,contentDTO);
            dto.setProduct(contentDTO);
            dto.setOrderId(orderId);
            dto.setCallbackId(asyncOrderCallback.getId());
            //优惠券价格
            if(orderCoupon != null && orderCoupon.getCouponTotalPrice() != null){
                dto.setCouponTotalPrice(orderCoupon.getCouponTotalPrice());
                dto.setCouponIds(orderCoupon.getCouponIds());
            }
            publisher.publishEvent(new AsyncCreateEvent(dto,source,true));
            logger.info("callback publish success");
            result.put("id", asyncOrderCallback.getId());
            return result;
        }catch (Exception e) {
            logger.info("asyncOrderCallback" +e.getMessage());
            e.printStackTrace();
            result.put("error", "callback创建失败");
            return result;
        }finally {
            redisTemplate.delete(cacheKey);
        }
    }

    /**
     *
     * @param product
     * @param prodType
     * @return 0 商品没path，1商品设置path成功
     */
    private Integer getPath(ProductFormDTO product, Integer prodType) {
        Integer keyId  = new Integer(0);
        // 59 authedPath
        if(prodType == HotWordCategoryEnum.IMAGE.getCode()){
            keyId = 59;
        }
        //32 path
        if(prodType == HotWordCategoryEnum.MUSIC.getCode()){
            keyId = 32;
        }

        ProductAttributeExample attrExample = new ProductAttributeExample();
        attrExample.createCriteria().andProductIdEqualTo(product.getId()).andAttrKeyIdEqualTo(keyId);
        List<ProductAttribute>  attrList = productAttributeDao.selectByExample(attrExample);
        if(attrList == null || attrList.size() ==0){
            logger.info("product does not have path "+product.toString() );
            return 0;
        }

        product.setPath(attrList.get(0).getAttrValue());
        return 1;
    }

    @Override
    public Map<String, Object> isOrderCallbackSuccessById(Integer id) throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        if(id == null){
            result.put("error","参数id不能为空");
        }
        String cacheKey = "asyncOrderCallback:"+id;
        Integer status = 0;
        Integer callbackId = null;
        String errorMsg = null;
        String redisValue = redisTemplate.opsForValue().get(cacheKey);
        if (StringUtils.isNotBlank(redisValue)) {
            ObjectMapper mapper = new ObjectMapper();
            CallBackStatusDto statusDto = mapper.readValue(redisValue, CallBackStatusDto.class);
            status = statusDto.getStatus();
            callbackId = statusDto.getCallbackId();
            errorMsg = statusDto.getMessage();
        } else {

            AsyncOrderCallback callback = asyncOrderCallbackService.selectByPrimaryKey(id);
            if(callback == null){
                status = 3;
            }else{
                status = callback.getStatus().intValue();
                callbackId = callback.getExternalId();
                errorMsg = callback.getErrorMsg();
            }
            CallBackStatusDto statusDto = new CallBackStatusDto();
            statusDto.setStatus(status);
            statusDto.setCallbackId(id);
            statusDto.setMessage(errorMsg);
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(statusDto);
            redisTemplate.opsForValue().set(cacheKey,json,10, TimeUnit.MINUTES);

        }
        Map map = new HashMap();
        map.put("status",status);
        map.put("callBackId",callbackId);
        map.put("error",errorMsg);
        return map;
    }

}
