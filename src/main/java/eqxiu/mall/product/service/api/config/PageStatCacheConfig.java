package eqxiu.mall.product.service.api.config;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class PageStatCacheConfig {
    @Value("${spring.pvredis.host}")
    private String host;

    @Value("${spring.pvredis.port}")
    private int port;

    @Value("${spring.pvredis.database}")
    private int database;
    
    @Value("${spring.pvredis.password}")
    private String password;

    @Value("${spring.pvredis.pool.max-active}")
    private int maxTotal;
    
    @Value("${spring.pvredis.pool.max-idle}")
    private int maxIdle;
    
    @Value("${spring.pvredis.pool.min-idle}")
    private int minIdle;

    @Value("${spring.pvredis.pool.max-wait}")
    private long maxWaitMillis;
    
    @Value("${spring.pvredis.pool.time-between-eviction-runs-millis}")
    private long timeBetweenEvictionRunsMillis;

    @Value("${spring.pvredis.pool.min-evictable-idle-timemillis}")
    private long minEvictableIdleTimeMillis;

    @Bean(name = "pageStatCache", autowire = Autowire.BY_NAME)
    public StringRedisTemplate redisTemplate() {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(maxTotal);
        poolConfig.setMaxWaitMillis(maxWaitMillis);
        poolConfig.setMaxIdle(maxIdle);
        poolConfig.setTestOnBorrow(false);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        poolConfig.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        
        JedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory(poolConfig);
        redisConnectionFactory.setHostName(host);
        redisConnectionFactory.setPort(port);
        redisConnectionFactory.setDatabase(database);
        redisConnectionFactory.setPassword(password);
        
        redisConnectionFactory.setUsePool(true);
        redisConnectionFactory.afterPropertiesSet();

        StringRedisTemplate template = new StringRedisTemplate(redisConnectionFactory);
//        template.setConnectionFactory(pageStatRedisConnectionFactory());
        
//        template.afterPropertiesSet();
        return template;
    }

}
