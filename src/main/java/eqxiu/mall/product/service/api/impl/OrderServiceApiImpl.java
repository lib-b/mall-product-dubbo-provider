package eqxiu.mall.product.service.api.impl;

import eqxiu.mall.commons.constants.OrderStatusEnum;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.order.model.Order;
import eqxiu.mall.order.model.OrderExample;
import eqxiu.mall.order.service.OrderService;
import eqxiu.mall.product.dto.OrderDTO;
import eqxiu.mall.product.dto.PageDTO;
import eqxiu.mall.product.exception.ApiException;
import eqxiu.mall.product.model.Product;
import eqxiu.mall.product.service.ProductAttributeService;
import eqxiu.mall.product.service.ProductService;
import eqxiu.mall.product.service.api.OrderServiceApi;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

public class OrderServiceApiImpl implements OrderServiceApi {

    @Autowired
    private OrderService orderService;

    @Autowired
    private ProductAttributeService productAttributeService;
    @Autowired
    private ProductService productService;

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceApiImpl.class);

    @Override
    public void saveOrder(OrderDTO orderDto) throws ApiException {

        Integer prodId = orderDto.getProductId();
        String outOrderId = orderDto.getOutOrderId();
        String createUser = orderDto.getCreateUser();

        if (outOrderId == null || prodId == null || createUser == null) {
            final String msg = "productId or createUser or outOrderId is null";
            logger.error(msg);
            throw new ApiException(msg);
        }

        Order order = new Order();

        BeanUtils.copyProperties(orderDto, order);

        if (orderDto.getStatus() == null) {
            order.setStatus(OrderStatusEnum.PAID.status());
        }

        order.setCreateTime(new Date());
        // order.set
        Product product = productService.getProduct(prodId);

        if (product == null) {
            throw new ApiException("product " + prodId + "not found!");
        }

        OrderExample example = new OrderExample();
        example.createCriteria().andOutOrderIdEqualTo(outOrderId);
        if (orderService.countByExample(example) > 0) {
            throw new ApiException("outOrderId " + outOrderId + " already exist!");
        }

        orderService.saveOrder(order);
    }

    @Override
    public Map<String, Object> saveOrderNew(OrderDTO orderDto) throws ApiException {

        Integer prodId = orderDto.getProductId();
        String outOrderId = orderDto.getOutOrderId();
        String createUser = orderDto.getCreateUser();

        if (outOrderId == null || prodId == null || createUser == null) {
            final String msg = "productId or createUser or outOrderId is null";
            logger.error(msg);
            throw new ApiException(msg);
        }

        Order order = new Order();

        BeanUtils.copyProperties(orderDto, order);

        if (orderDto.getStatus() == null) {
            order.setStatus(OrderStatusEnum.PAID.status());
        }

        order.setCreateTime(new Date());
        // order.set
        Product product = productService.getProduct(prodId);

        if (product == null) {
            throw new ApiException("product " + prodId + "not found!");
        }

        OrderExample example = new OrderExample();
        example.createCriteria().andOutOrderIdEqualTo(outOrderId);
        if (orderService.countByExample(example) > 0) {
            throw new ApiException("outOrderId " + outOrderId + " already exist!");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object>  productWithAuthed = productService.getProductMapWithAuthed(prodId);
        orderService.saveOrder(order);
        map.put("orderId",order.getId());
        map.put("product",productWithAuthed);
        return map;
    }

    @Override
    public List<Map<String, Object>> getMyProducts(String userId) {
        OrderExample example = new OrderExample();
        example.createCriteria().andCreateUserEqualTo(userId);
        example.setDistinct(true);
        example.setOrderByClause("create_time desc");
        List<Order> orders = orderService.selectByExample(example);

        List<Map<String, Object>> products = new ArrayList<Map<String, Object>>();
        for (Order order : orders) {
            Integer productId = order.getProductId();
            Map<String, Object> one = productService.getProductMapWithAuthed(productId);
            products.add(one);
        }

        return products;
    }

    @Override
    public PageDTO<Map<String, Object>> getMyProducts(String userId, Integer type, Integer pageNo, Integer pageSize) throws ApiException {
        if (userId == null) {
            throw new ApiException("userId can not be null");
        }

        OrderExample example = new OrderExample();
        example.createCriteria().andCreateUserEqualTo(userId);
        List<Order> orders = orderService.selectPageByExampleAndType(example,type,pageNo,pageSize);

        PageDTO<Map<String, Object>> paging = new PageDTO<Map<String, Object>>();

        List<Map<String, Object>> products = new ArrayList<Map<String, Object>>();
        if(orders != null){
            for (Order order : orders) {
                Integer productId = order.getProductId();
                Map<String, Object> one = productService.getProductMapWithAuthed(productId);
                products.add(one);
            }
            paging.setList(products);
            paging.setCount(orderService.countByExampleAndTyp(example,type));
        }else{
            paging.setCount(0);
        }

        return paging;
    }

    @Override
    public boolean hasBuyProduct(String userId, Integer productId) throws ApiException {
        if (userId == null || productId == null) {
            throw new ApiException("userId or productId is null");
        }

        OrderExample example = new OrderExample();
        example.createCriteria().andCreateUserEqualTo(userId).andProductIdEqualTo(productId);

        return orderService.countByExample(example) > 0;
    }

    @Override
    public List<OrderDTO> getBuyProductOrderList(String userId, Integer productId) throws ApiException {
        if (userId == null || productId == null) {
            throw new ApiException("userId or productId is null");
        }
        OrderExample example = new OrderExample();
        example.createCriteria().andCreateUserEqualTo(userId).andProductIdEqualTo(productId);
        List<Order> orderList =  orderService.selectByExample(example);
        List<OrderDTO> dtoList = new ArrayList<OrderDTO>();
        for(int i=0; i<orderList.size(); i++){
            Order o = orderList.get(i);
            OrderDTO dto = new OrderDTO();
            BeanUtils.copyProperties(o,dto);
            dtoList.add(dto);
        }
        return  dtoList;
    }


    @Override
    public List<Integer> getBuyProductOrderIdList(String userId, Integer productId) throws ApiException {
        if (userId == null || productId == null) {
            throw new ApiException("userId or productId is null");
        }
        List<Integer> idList = orderService.getBuyProductOrderIdList(userId, productId);
        return  idList;
    }


}
