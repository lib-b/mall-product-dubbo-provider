package eqxiu.mall.product.service.api.impl;

import org.springframework.beans.factory.annotation.Autowired;

import eqxiu.mall.product.dto.SceneTemplateDTO;
import eqxiu.mall.product.service.SceneTemplateService;
import eqxiu.mall.product.service.api.SceneTemplateServiceApi;

public class SceneTemplateServiceApiImpl implements SceneTemplateServiceApi {

    @Autowired
    private SceneTemplateService sceneTemplateService;

    @Override
    public void addScene(SceneTemplateDTO dto) throws Exception {
        sceneTemplateService.addScene(dto);
    }



}
