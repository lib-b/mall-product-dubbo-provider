package eqxiu.mall.product.service.api.impl;

import eqxiu.mall.product.dto.CategoryDTO;
import eqxiu.mall.product.model.Category;
import eqxiu.mall.product.service.CategoryService;
import eqxiu.mall.product.service.api.CategoryServiceApi;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class CategoryServiceApiImpl implements CategoryServiceApi {
    @Autowired
    private CategoryService categoryService;

    @Override
    public List<CategoryDTO> getChildList(Integer id) {
        List<Category> categories = categoryService.iterateChild(id);
        List<CategoryDTO> categoryDTOs = new ArrayList<CategoryDTO>();
        for (Category category : categories) {

            CategoryDTO dto = new CategoryDTO();

            BeanUtils.copyProperties(category, dto);

            List<Category> children = category.getChilds();
            List<CategoryDTO> childrenDTO = new ArrayList<CategoryDTO>();
            if (children != null) {
                for (Category cate : children) {
                    CategoryDTO dtoChild = new CategoryDTO();
                    BeanUtils.copyProperties(cate, dtoChild);
                    childrenDTO.add(dtoChild);
                }
                dto.setChildren(childrenDTO);
            }

            categoryDTOs.add(dto);
        }

        return categoryDTOs;
    }

}
