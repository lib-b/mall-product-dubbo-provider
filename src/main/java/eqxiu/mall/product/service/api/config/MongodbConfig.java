/*
package eqxiu.mall.product.service.api.config;


import com.mongodb.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.WriteResultChecking;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class MongodbConfig {

    @Value("${spring.mongo.uri}")
    private String uri;

    @Value("${spring.mongo.connectionsPerHost}")
    private int connectionsPerHost;

    @Value("${spring.mongo.threadsAllowedToBlockForConnectionMultiplier}")
    private int threadsAllowedToBlockForConnectionMultiplier;

    @Value("${spring.mongo.connectTimeout}")
    private int connectTimeout;

    @Value("${spring.mongo.maxWaitTime}")
    private int maxWaitTime;

    @Value("${spring.mongo.socketKeepAlive}")
    private Boolean socketKeepAlive;

    @Value("${spring.mongo.socketTimeout}")
    private int socketTimeout;

    @Value("${spring.mongo.minConnectionsPerHost}")
    private int minConnectionsPerHost;

    @Value("${spring.mongo.heartbeatConnectTimeout}")
    private int heartbeatConnectTimeout;

    @Value("${spring.mongo.maxConnectionIdleTime}")
    private int maxConnectionIdleTime;



    @Bean
    public MongoTemplate mongoTemplate() throws Exception{

        MongoClientURI mongoUri = new MongoClientURI(uri);
        MongoDbFactory factory = new SimpleMongoDbFactory(mongoUri);
        MappingMongoConverter converter =
                new MappingMongoConverter(factory, new MongoMappingContext());
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));
        MongoTemplate mongoTemplate = new MongoTemplate(factory,converter);
        mongoTemplate.setWriteResultChecking(WriteResultChecking.EXCEPTION);
        return mongoTemplate;
    }

    @Bean
    public MongoClientOptions mongoOptions(){
        MongoClientOptions options = new MongoClientOptions.Builder().socketKeepAlive(socketKeepAlive)
                .connectTimeout(connectTimeout) // 链接超时时间
                .socketTimeout(socketTimeout) // read数据超时时间
                .threadsAllowedToBlockForConnectionMultiplier(threadsAllowedToBlockForConnectionMultiplier)
                .connectionsPerHost(connectionsPerHost) // 每个地址最大请求数
                .maxWaitTime(maxWaitTime) // 长链接的最大等待时间
                .minConnectionsPerHost(minConnectionsPerHost)
                .heartbeatConnectTimeout(heartbeatConnectTimeout)
                .maxConnectionIdleTime(maxConnectionIdleTime)
                .readPreference(ReadPreference.secondaryPreferred())
                .build();
        return options;
    }

}
*/
