package eqxiu.mall.product.service.api.impl;

import eqxiu.mall.product.dao.ArtistStatisticsDao;
import eqxiu.mall.product.dto.ArtistDTO;
import eqxiu.mall.product.exception.ApiException;
import eqxiu.mall.product.model.Artist;
import eqxiu.mall.product.service.ArtistService;
import eqxiu.mall.product.service.ArtistStatisticsService;
import eqxiu.mall.product.service.ProductService;
import eqxiu.mall.product.service.api.ArtistServiceApi;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

public class ArtistServiceApiImpl implements ArtistServiceApi {

    @Autowired
    private ArtistService artistService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ArtistStatisticsService artistStatisticsService;
    
    @Override
    public void addArtist(ArtistDTO dto) {

        Artist record = new Artist();
        BeanUtils.copyProperties(dto, record);
        Artist artist = artistService.findArtistByUUID(dto.getUserId());
        if(artist == null){
            artistService.insertSelective(record);
        }else{
            record.setId(artist.getId());
            artistService.updataSelective(record);
        }
    }

    @Override
    public ArtistDTO getArtistById(Integer id) {
        Artist artist = artistService.getAvatarByPrimaryKey(id);
        if (artist != null) {
            ArtistDTO artistDto = new ArtistDTO();
            BeanUtils.copyProperties(artist, artistDto);
            artistDto.setName(artist.getArtistName());
            artistDto.setUserId(artist.getArtistUid());
            artistDto.setHeadImg(artist.getAvatar());
            
            return artistDto;
        }
        return null;

    }

    @Override
    public Map<String, Object> getArtistSellStatistics(String userId,String startTime, String endTime) {
        if(userId == null){
            throw new RuntimeException("用户ID不能为空");
        }
        Map<String, Object> result = new HashMap<String,Object>();
        Artist artist = artistService.findArtistByUUID(userId);
        //应要求，如果用户不存在，返回0
        if(artist == null){
            result.put("pcSales",0);
            result.put("appSales",0);
            result.put("userId",userId);
            result.put("artistId",null);
            return result;
        }
        result =  artistStatisticsService.getArtistSellStatistics(artist.getId(), startTime, endTime);
        if(result == null || result.size() == 0){
            result = new HashMap<String, Object>();
            result.put("pcSales",0);
            result.put("appSales",0);
        }

        result.put("artistId",artist.getId());
        result.put("userId",userId);
        return result;
    }

}
