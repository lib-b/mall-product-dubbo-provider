package eqxiu.mall.product.service.api.impl;


import eqxiu.mall.product.dto.SpecialDTO;
import eqxiu.mall.product.exception.ApiException;
import eqxiu.mall.product.model.Special;
import eqxiu.mall.product.service.SpecialProductService;
import eqxiu.mall.product.service.SpecialService;
import eqxiu.mall.product.service.api.SpecialServiceApi;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SpecialServiceApiImpl implements SpecialServiceApi {

    @Autowired
    private SpecialService specialService;
    @Autowired
    private SpecialProductService specialProductService;

    @Override
    public List<SpecialDTO> specialList(Byte type) throws ApiException{
        if(type == null){
            throw new ApiException("type can not be null!");
        }
        List<Special> result = specialService.findUsedSpecials(type);
        List<SpecialDTO> specialDTOList = new ArrayList<SpecialDTO>();
        if(result != null && result.size() > 0){
            for(int i=0; i<result.size(); i++){
                SpecialDTO dto = new SpecialDTO();
                BeanUtils.copyProperties(result.get(i),dto);
                specialDTOList.add(dto);
            }
        }else{
            specialDTOList = null;
        }
        return specialDTOList;
    }

    @Override
    public List<Map<String, Object>> specialDetails(Integer specialId) throws ApiException{
        List<Map<String, Object>> products = specialProductService.getProductBySpecial(specialId,true);
        return products;
    }
}
