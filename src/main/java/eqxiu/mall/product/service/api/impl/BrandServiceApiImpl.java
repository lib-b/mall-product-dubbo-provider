package eqxiu.mall.product.service.api.impl;

import eqxiu.mall.product.dto.BrandDTO;
import eqxiu.mall.product.model.Brand;
import eqxiu.mall.product.service.BrandService;
import eqxiu.mall.product.service.api.BrandServiceApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class BrandServiceApiImpl implements BrandServiceApi {
	@Autowired
	private BrandService brandService;
	private final Logger logger = LoggerFactory.getLogger(BrandServiceApiImpl.class);

	@Override
	public BrandDTO getBrand(int brandId) {

		BrandDTO dto = new BrandDTO();
		try {
			Brand brand = brandService.findByID(brandId);
			BeanUtils.copyProperties(brand,dto);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return dto;
	}

	@Override
	public BrandDTO getBrandByProdId(int productId) {
		BrandDTO dto = new BrandDTO();

		Brand brand = brandService.findOneByProdId(productId);
		BeanUtils.copyProperties(brand, dto);

		return dto;
	}

}
