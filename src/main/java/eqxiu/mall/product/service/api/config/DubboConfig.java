package eqxiu.mall.product.service.api.config;

import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Component;

@Component
@ImportResource(locations={"classpath:dubbo-*.xml"})
public class DubboConfig {
}
