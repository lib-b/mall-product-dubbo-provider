package eqxiu.mall.product.service.api.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.product.dao.ProductStatisticsDao;
import eqxiu.mall.product.dto.BrandDTO;
import eqxiu.mall.product.dto.CategoryDTO;
import eqxiu.mall.product.dto.PageDTO;
import eqxiu.mall.product.dto.TagDTO;
import eqxiu.mall.product.model.*;
import eqxiu.mall.product.service.*;
import eqxiu.mall.product.service.api.ProductServiceApi;
import eqxiu.search.searcher.api.MallSearchApi;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @description 对外开放
 * @author ranger
 * @date Sep 20, 2016
 */
public class ProductServiceApiImpl implements ProductServiceApi {

	@Autowired
	private ProductService productService;

	@Autowired
	private TagService tagService;

	@Autowired
	private BrandService brandService;
	@Autowired
	private CategoryProductService cateProdService;
	@Autowired
	private MallSearchApi mallSearchApi;

	@Autowired
	private ProductStatisticsDao productStatisticsDao;

	private final Logger logger = LoggerFactory.getLogger(BrandServiceApiImpl.class);

	@Override
	public Map<String, Object> getProductDetail(int productId) {

		Map<String, Object> prodMap = productService.getProductMap(productId);
		return prodMap;
	}

	@Override
	public Map<String, Object> getProductDetailWithAuthed(int productId) {

		Map<String, Object> prodMap = productService.getProductMapWithAuthed(productId);
		return prodMap;
	}

	@Override
	public Map<String, Object> getProductLite(int productId) {

		Map<String, Object> prodMap = productService.getProductMapLite(productId);

		return prodMap;
	}

	@Override
	public List<TagDTO> getTagsByProdId(int productId, Integer count) {
		List<TagDTO> result = new ArrayList<>();
		List<Tag> tags = tagService.findByProdId(productId, count);
		if (tags != null) {
			for (Tag tag : tags) {
				TagDTO dto = new TagDTO();
				BeanUtils.copyProperties(tag, dto);
				result.add(dto);
			}
		}
		return result;
	}

	@Override
	public List<TagDTO> getTagsByProdId(int productId) {
		List<TagDTO> result = new ArrayList<>();
		List<Tag> tags = tagService.findByProdId(productId, -1);
		if (tags != null) {
			for (Tag tag : tags) {
				TagDTO dto = new TagDTO();
				BeanUtils.copyProperties(tag, dto);
				result.add(dto);
			}
		}
		return result;
	}

	@Override
	public List<CategoryDTO> getCategoryByProdId(int productId) {
		List<Category> categories = cateProdService.findByProductId(productId);
		List<CategoryDTO> categoriesDTO = new ArrayList<CategoryDTO>();
		for (Category cate : categories) {
			CategoryDTO dto = new CategoryDTO();
			BeanUtils.copyProperties(cate, dto);
			categoriesDTO.add(dto);
		}

		return categoriesDTO;
	}

	@Override
	public PageDTO<Map<String, Object>> getProductByCategory(Integer category, int pageNo, int pageSize) {
//		eqxiu.mall.commons.page.PageDTO<Map<String, Object>> pageDTO = productService.getProductByCategory(category,
//				null, pageNo, pageSize, true);
		Map<String, Object> paramMap = new HashMap<String, Object>();

		paramMap.put("category", category.toString());
		paramMap.put("keywords", "");
		paramMap.put("sortBy", "sort|desc");

		PageDTO<Map<String, Object>> pageDTO = new PageDTO<Map<String, Object>>();
		try {
			eqxiu.search.searcher.model.PageDTO<Map<String, Object>> prodTmp;
			prodTmp = mallSearchApi.search(paramMap, pageNo, pageSize);
			if (prodTmp != null) {
				pageDTO.setCount(prodTmp.getTotal());
				pageDTO.setList(prodTmp.getList());
				pageDTO.setPageNo(prodTmp.getPageNo());
				pageDTO.setPageSize(prodTmp.getPageSize());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return pageDTO;
	}

	/**
	 *
	 * @param category 分类ID
	 * @param pageNo 页码
	 * @param pageSize 最多展示数量
	 * @param param 扩展参数
	 *              key="sort"  value="create_time|desc"
	 *              key="priceRange" value="0a0" 免费 "1a" 付费
	 *
	 * @return
	 */
	@Override
	public PageDTO<Map<String, Object>> getProductByCategory(Integer category, int pageNo, int pageSize, Map<String, String> param) {
		Map<String, Object> paramMap = new HashMap<String, Object>();

		paramMap.put("category", category.toString());
		paramMap.put("keywords", "");
		String sortBy = param.get("sort");
		if(StringUtils.isBlank(sortBy)){
			paramMap.put("sortBy", "sort|desc");
		}else{
			paramMap.put("sortBy", sortBy);
		}

		String priceRange = param.get("priceRange");
		if(StringUtils.isNotBlank(priceRange)){
			extractPrice(paramMap, priceRange);
		}

		PageDTO<Map<String, Object>> pageDTO = new PageDTO<Map<String, Object>>();
		try {
			eqxiu.search.searcher.model.PageDTO<Map<String, Object>> prodTmp;
			prodTmp = mallSearchApi.search(paramMap, pageNo, pageSize);
			if (prodTmp != null) {
				pageDTO.setCount(prodTmp.getTotal());
				pageDTO.setList(prodTmp.getList());
				pageDTO.setPageNo(prodTmp.getPageNo());
				pageDTO.setPageSize(prodTmp.getPageSize());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return pageDTO;
	}

	private void extractPrice(Map<String, Object> paramMap, final String priceRange) {
		String[] prices = priceRange.split("a");
		int length = prices.length;
		if (length == 2) {
			paramMap.put("priceFloor", prices[0]);
			paramMap.put("priceCeiling", prices[1]);
		} else if (length == 1) {
			paramMap.put("priceFloor", prices[0]);
		}
	}

	@Override
	public Integer getProductCountByAttrGroupId(Date startDay, int attrGroupId) {
		// 查询截止到当前日期的所有商品数量
		ProductExample productExample = new ProductExample();
		productExample.createCriteria().andCreateTimeLessThanOrEqualTo(startDay).andAttrGroupIdEqualTo(attrGroupId);
		return (int) productService.countByExample(productExample);
	}

	@Override
	public Map<Integer, Map<String, Object>> getList(String ids) {
		JSONObject o = JSON.parseObject(ids);
		JSONArray array = o.getJSONArray("id");
		Map<Integer, Map<String, Object>> map = new HashMap<Integer, Map<String, Object>>();
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				Integer productId = (Integer) array.get(i);
				Map<String, Object> data = productService.getProductMap(productId);
				map.put(productId, data);
			}
		}
		return map;
	}

	@Override
	public Map<Integer, Map<String, Object>> getListBySourceId(String ids) {
		JSONObject o = JSON.parseObject(ids);
		JSONArray array = o.getJSONArray("id");
		Integer attrGroupId = o.getInteger("attrGroupId");
		Map<Integer, Map<String, Object>> map = new HashMap<Integer, Map<String, Object>>();
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				Integer productId = (Integer) array.get(i);
				Map<String, Object> data = productService.getProductMapBySourceId(productId,attrGroupId);
				if(data != null) {
					BrandDTO dto = new BrandDTO();
					Integer brandId = (Integer) data.get("brandId");
					Brand brand = brandService.findByID(brandId);
					try {
						BeanUtils.copyProperties(brand, dto);
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
					data.put("brand", dto);
				}
				map.put(productId, data);
			}
		}
		return map;
	}

	@Override
	public Map<String, Object> getProductStatistics(String code) {
		if(code == null){
			throw new RuntimeException("code不能为空");
		}
		Map<String,Object> result = new HashMap<String,Object>();
		Product product = productService.selectByCode(code);
		if(product == null){
			result.put("code",code);
			result.put("sourceId",null);
			result.put("id",null);
			result.put("sales",0);
			result.put("price",0);
			result.put("usage",0);
			return result;
		}
		ProductStatistics statistics = productStatisticsDao.selectByPrimaryKey(product.getId().longValue());

		result.put("code",code);
		result.put("sourceId",product.getSourceId());
		result.put("id",product.getId());
		result.put("price",product.getPrice());
		if(statistics == null){
			result.put("sales",0);
			result.put("usage",0);
		}else{
			if(statistics.getProductSales() == null){
				result.put("sales",0);
			}else{
				result.put("sales",statistics.getProductSales());
			}

			if(statistics.getProductUsage() == null){
				result.put("usage",0);
			}else{
				result.put("usage",statistics.getProductUsage());
			}
		}
		return result;
	}

	@Override
	public Map<String, Map<String, Object>> getFontListByFamily(String fontParam) {
		JSONObject o = JSON.parseObject(fontParam);
		JSONArray array = o.getJSONArray("fontFamily");
		Integer priceType = o.getInteger("priceType");

		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();

		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				String fontFamily = (String) array.get(i);
				Integer fontId = productService.getFontByFamily(fontFamily, priceType);
				Map<String, Object> data = null;
				if (fontId != null) {
					data = productService.getProductMap(fontId);
					if (data != null) {
						BrandDTO dto = new BrandDTO();
						Integer brandId = (Integer) data.get("brandId");
						Brand brand = brandService.findByID(brandId);
						try {
							BeanUtils.copyProperties(brand, dto);
						} catch (Exception e) {
							logger.error(e.getMessage());
						}
						data.put("brand", dto);
					}
				} 
				map.put(fontFamily, data);
			}
		}
		return map;
	}

}
